#!/bin/bash -eu

# $ xcode-select --install
# /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"


brew update
brew upgrade
brew doctor
# brew tap caskroom/versions
# brew update && brew upgrade brew-cask && brew cleanup && brew cask cleanup

# install some command line tools
brew install wget
brew install tree
brew install git
brew install zsh

# install some apps
brew cask install google-chrome
brew cask install firefox
brew cask install sourcetree
brew cask install google-japanese-ime
brew cask install virtualbox
# brew install pyenv
# brew install pyenv-virtualenv

# if you need
brew cask install java
brew cask install sublime-text

